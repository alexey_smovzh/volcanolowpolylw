package VolcanoLowPolyLW;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.LoopMode;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.light.DirectionalLight;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;

/*
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 *
 */

public class AirShip extends AbstractAppState {

    private SimpleApplication app;
    private AppStateManager stateManager;    
    private DirectionalLight light;
    private AssetManager assetManager;    
    private Node parent;    
    
    private Node model;
    private String modelsPath;                                                  // путь к файлу с моделями    
    
    private AnimControl control;
    private AnimChannel propeller;
    
    private float size = DEFAULT_AIRSHIPSIZE;                                   // масштабирование
    
    private final static float DEFAULT_AIRSHIPSIZE = 1f;
        
    private final static int FAR_PLANE = 0;                                     // коридор появления 
    private final static int NEAR_PLANE = 6;
    private final static int TOP_PLANE = 10;                                 
    private final static int BOTTOM_PLANE = 6; 
    private final static float FRUSTUM_ANGLE = FastMath.tan(FastMath.DEG_TO_RAD * 45f);
    private boolean windDirection;                                              // направление движения
    
    private final static Vector3f WIND_SPEED_R2L = new Vector3f(0f, 0f, 0.7f);
    private final static Vector3f WIND_SPEED_L2R = new Vector3f(0f, 0f, -0.7f);    


    // Constructor
    public AirShip(DirectionalLight light) {
        
        this.light = light;
        
    }
    
    // контрол класс, движение 
    private class AirShipControl extends AbstractControl {
        
        private final Node parent;
        private Vector3f moveStep;
        private float frustum;
        
        
        // Control
        public AirShipControl(Node parent) {
            
            this.parent = parent;     
            
        }
        
        @Override 
        public void setSpatial(Spatial spatial) {
            
            super.setSpatial(spatial);
            
            // Точка за пределами фрустума для изчезновения 
            // скорость и направление полета против ветра       
            if(windDirection) {
                moveStep = WIND_SPEED_L2R;                
            } else {
                moveStep = WIND_SPEED_R2L;                
            }                   

            // потому что минус на минус дает плюс
            frustum = -spatial.getLocalTranslation().z;

        }

        @Override
        protected void controlUpdate(float tpf) {
  
            spatial.move(moveStep.mult(tpf));

            // удаление когда дирижабль выйдет за фруструм
            if(windDirection) {
                if(spatial.getLocalTranslation().z < frustum) {
                    parent.detachChild(spatial);
        
                    // Set stop short animation flag
                    stateManager.getState(AnimationManager.class).setShortAnimationStop();                
                }                    
            } else {
                if(spatial.getLocalTranslation().z > frustum) {
                    parent.detachChild(spatial);                
                
                    // Set stop short animation flag
                    stateManager.getState(AnimationManager.class).setShortAnimationStop();                
                }
            }                         
        }

        @Override
        protected void controlRender(RenderManager rm, ViewPort vp) { }        
        
    }
    
    // Точка появления дирижабля за пределами фрустума
    private Vector3f getInitialPosition() {
        
        float x = (float)FastMath.nextRandomInt(FAR_PLANE, NEAR_PLANE);
        float y = (float)FastMath.nextRandomInt(BOTTOM_PLANE, TOP_PLANE);
        float z = (FRUSTUM_ANGLE * x) + 8f;        
        
        if(!windDirection) {
            return new Vector3f(x, y, -z);
        } else {
            return new Vector3f(x, y, z);            
        }        
    }

    private void loadModel() {      

        // Load airship
        model = (Node)assetManager.loadModel(modelsPath);
        model.setShadowMode(RenderQueue.ShadowMode.Cast);
        model.addLight(light);

        if(size != DEFAULT_AIRSHIPSIZE) { model.scale(size); }                  // масштабируем               
        
    }
    
    public void playAnimation() {
        
        // Setup running animation flag
        stateManager.getState(AnimationManager.class).setShortAnimationRun();

        // Wind direction
        windDirection = stateManager.getState(AnimationManager.class).getWindDirection();        

        Node n = (Node)model.clone();
        
        n.setLocalTranslation(getInitialPosition());        
        n.addControl(new AirShipControl(parent));
            
        if(windDirection) n.rotate(0f, FastMath.DEG_TO_RAD * 180f, 0f);         // при движении слева направо дирижабль разворачиваем вокруг Y на 180 градусов        
        
        // Animation
        control = n.getChild("AirShip").getControl(AnimControl.class);
        propeller = control.createChannel();
        propeller.setAnim("Propeller");
        propeller.setLoopMode(LoopMode.Loop);
      
        parent.attachChild(n);
        
    }
    
    // Setters
    public void setModelsPath(String path) { this.modelsPath = path; }          // Файл с моделями        
    public void setSize(float size) { this.size = size; }                       // Масштабирование
    public void setParent(Node parent) { this.parent = parent; }                // Родитель
    
    
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        
        super.initialize(stateManager, app);
        
        this.app = (SimpleApplication)app;
        this.assetManager = this.app.getAssetManager();
        this.stateManager = this.app.getStateManager();
        
        if(this.modelsPath == null) throw new NullPointerException("(String)path not set.");
        
        if(parent == null) { parent = this.app.getRootNode(); }  
        
        loadModel();
        
    }        
}
