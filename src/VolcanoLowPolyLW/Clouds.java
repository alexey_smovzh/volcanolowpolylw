/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package VolcanoLowPolyLW;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.light.DirectionalLight;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import java.util.List;

/*
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 *
 */

public class Clouds extends AbstractAppState {
    
    private SimpleApplication app;
    private AppStateManager stateManager;
    private Node parent;
    private AssetManager assetManager;    
    private DirectionalLight light;
    private Camera cam;
    
    private String modelsPath;                                                  // путь к файлу с моделями
    private List<Spatial> models;                                               // список с моделями частиц    
    
    private final static int FAR_PLANE = -2;                                    // коридор появления облаков
    private final static int NEAR_PLANE = 4;
    private final static int TOP_PLANE = 10;                                 
    private final static int BOTTOM_PLANE = 6; 
    private final static float FRUSTUM_ANGLE = FastMath.tan(FastMath.DEG_TO_RAD * 45f);
    private boolean windDirection;                                              // направление движения облаков
    
    private final static Vector3f WIND_SPEED_R2L = new Vector3f(0f, 0f, 0.5f);
    private final static Vector3f WIND_SPEED_L2R = new Vector3f(0f, 0f, -0.5f);    
    
    public byte cloudsState = STOPPED_CLOUDSSTATE;
    public final static byte STOPPED_CLOUDSSTATE = 0;
    public final static byte START_CLOUDSSTATE = 1;
    public final static byte STOP_CLOUDSSTATE = 2;    
    public final static byte WORKING_CLOUDSSTATE = 3;        
    
    private float update = 0f;
    
    private float density = DEFAULT_DENSITY;                                    // "густота" облаков
    private float size = DEFAULT_CLOUDSIZE;                                     // масштабирование
    
    private static final float DEFAULT_DENSITY = 4f;                            // чем значение больше тем реже облака появляются
    private static final float DEFAULT_CLOUDSIZE = 1f;    
    
   
    // Constructor
    public Clouds(DirectionalLight light) {
        
        this.light = light;
        
    }
    

    // контрол класс, движение облаков
    private class CloudControl extends AbstractControl {
        
        private final Node parent;
        private DirectionalLight light;
        private Vector3f moveStep;
        private float frustum;
        
        
        // Control
        public CloudControl(Node parent, DirectionalLight light) {
            
            this.parent = parent;  
            this.light = light;            
            
        }
        
        @Override 
        public void setSpatial(Spatial spatial) {
            
            super.setSpatial(spatial);
            
            // Точка за пределами фрустума для изчезновения облаков
            // скорость и направление полета            
            if(windDirection) {
                moveStep = WIND_SPEED_R2L;
            } else {            
                moveStep = WIND_SPEED_L2R;                
            }               
            
            frustum = -spatial.getLocalTranslation().z;    
        }

        @Override
        protected void controlUpdate(float tpf) {
  
            spatial.move(moveStep.mult(tpf));
            
            light.setDirection(cam.getDirection());
            
            // удаления когда облако выйдет за фруструм
            if(windDirection) {
                if(spatial.getLocalTranslation().z > frustum)
                    parent.detachChild(spatial);
            } else {
                if(spatial.getLocalTranslation().z < frustum)
                    parent.detachChild(spatial);                
            }             
        }

        @Override
        protected void controlRender(RenderManager rm, ViewPort vp) { }        
        
    }
    
    // Start clouds
    private void cloudsOn() {
        
        windDirection = stateManager.getState(AnimationManager.class).getWindDirection();
        
    }
    
    // Stop clouds
    private void cloudsOff() {
        
    }
    
    // Загружаем модели облаков
    private void loadModels() {
        
        Node m = (Node)assetManager.loadModel(modelsPath);
        this.models = m.getChildren();
        
    }    
    
    // Clouds controls
    public void startClouds() { this.cloudsState = START_CLOUDSSTATE; }
    public void stopClouds() { this.cloudsState = STOPPED_CLOUDSSTATE; }

    public void setModelsPath(String path) { this.modelsPath = path; }          // Файл с моделями    
    public void setDensity(float density) { this.density = density; }           // "густота" облаков    
    public void setSize(float size) { this.size = size; }                       // Масштабирование
    public void setParent(Node parent) { this.parent = parent; }                // Родитель
    
    
    // Вызывается из update, порождает облака
    private void emit() {
        
        Node n = (Node)models.get(FastMath.rand.nextInt(models.size())).clone();
        n.setLocalTranslation(getInitialPosition());                            // точка создания облаков
        n.setShadowMode(RenderQueue.ShadowMode.Cast);
        n.setQueueBucket(RenderQueue.Bucket.Opaque);
        
        if(size != DEFAULT_CLOUDSIZE) { n.scale(size); }                        // масштабируем       

        n.addControl(new CloudControl(parent, light));
        n.addLight(light);
        parent.attachChild(n);
        
    }
      
    // Точка появления облака за пределами фрустума
    private Vector3f getInitialPosition() {
        
//      min, max
        float x = (float)FastMath.nextRandomInt(FAR_PLANE, NEAR_PLANE);
        float y = (float)FastMath.nextRandomInt(BOTTOM_PLANE, TOP_PLANE);
        float z = (FRUSTUM_ANGLE * x) + 6f;        
        
        if(windDirection) {
            return new Vector3f(x, y, -z);
        } else {
            return new Vector3f(x, y, z);            
        }        
    }
    
    
    @Override
    public void update(float tpf) {
        
        if(cloudsState == START_CLOUDSSTATE) { 
            cloudsOn();
            cloudsState = WORKING_CLOUDSSTATE;
        } else if(cloudsState == STOP_CLOUDSSTATE) {
            cloudsOff();
            cloudsState = STOPPED_CLOUDSSTATE;
        }
        
        if(cloudsState == WORKING_CLOUDSSTATE) {
            if(update > density) {
                emit();
                update = 0f;
            }

            update += tpf;        
        }
    }              

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        
        super.initialize(stateManager, app);
        
        this.app = (SimpleApplication)app;
        this.stateManager = this.app.getStateManager();
        this.assetManager = this.app.getAssetManager();
        this.cam = this.app.getCamera();
        
        if(this.modelsPath == null) throw new NullPointerException("(String)path not set.");
        
        if(parent == null) { parent = this.app.getRootNode(); }
        
        loadModels();
        
    }       
}

