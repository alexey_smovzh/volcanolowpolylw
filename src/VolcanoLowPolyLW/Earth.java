/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package VolcanoLowPolyLW;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

/*
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 *
 */

public class Earth extends AbstractAppState {
    
    private SimpleApplication app;
    private AppStateManager stateManager;
    private Node rootNode;
    private AssetManager assetManager;    
    private DirectionalLight light;

    private Node scene;
    private Node pivot;
    private Node bonfire;
    private ParticleEmitter3D fire;    
    private ParticleEmitter3D smoke;
    private final static Vector3f source = new Vector3f(-1.6f, 3f, 0.2f);
    private final static Vector3f bonfireSource = new Vector3f(1.3f, 0.2f, 1.45f);
    
    public byte eruption = STOPPED_STATE;
    public byte burning = STOPPED_STATE;    
    public final static byte STOPPED_STATE = 0;
    public final static byte START_STATE = 1;
    public final static byte STOP_STATE = 2;    
    public final static byte WORKING_STATE = 3;      
        

    private Vector3f volcanoSmokeDirection;
    private final static Vector3f VOLCANO_SMOKE_DIRECTION = new Vector3f(-2f, 8f, 1f);// в высоту 8f, назад на 2f
    

    // Constructor
    public Earth(DirectionalLight light) {
        
        this.light = light;
        
    }

    private void loadModels() {
     
        // Load scene & setting proper shadow mode
        scene = (Node)assetManager.loadModel("Blender/Volcano/volcano.j3o");
        Spatial earth = scene.getChild("Earth");
        earth.setShadowMode(RenderQueue.ShadowMode.Receive);
        
        Spatial mountain = scene.getChild("Mountain");
        mountain.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);
        
        Node lava = (Node)scene.getChild("Lava");
        lava.setShadowMode(RenderQueue.ShadowMode.Off);
        
        // Set lava transparency
/*        
        SceneGraphVisitor visitor = new SceneGraphVisitor() {

            public void visit(Spatial spatial) {
                if(spatial instanceof Geometry) {
                    Geometry geo = (Geometry)spatial;
                    geo.getMaterial().getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);
                }
            }            
        };
        
        lava.depthFirstTraversal(visitor);
        lava.setQueueBucket(RenderQueue.Bucket.Translucent);
*/        
        Spatial mill = scene.getChild("Mill");
        mill.setShadowMode(RenderQueue.ShadowMode.Cast);

        // Permanent animations
        // todo: !!!
        AnimControl millAnimControl = ((Node)((Node)mill).getChild("Mill")).getChild("Mill").getControl(AnimControl.class);
        AnimChannel millAnimChannel = millAnimControl.createChannel();
        millAnimChannel.setAnim("mill");
        
                
        scene.addLight(light);
        rootNode.attachChild(scene); 
        
        // Smoke emitter source
        pivot = new Node("Emitter");
        pivot.setLocalTranslation(source);
        scene.attachChild(pivot);  
        
        // Bonfire emitter source
        bonfire = new Node("Bonfire");
        bonfire.setLocalTranslation(bonfireSource);
        scene.attachChild(bonfire);
        
        DirectionalLight dl = new DirectionalLight();                           // "свечение" огня
        dl.setDirection(app.getCamera().getDirection());    
        dl.setColor(ColorRGBA.Yellow.mult(2f));
        
        bonfire.addLight(dl);
        
    }    

    // Burn bonfire
    private void bonefireOn() {
        
        fire = new ParticleEmitter3D();
        fire.setEmitter(bonfire);
        fire.setDirection(bonfireSource.mult(new Vector3f(0f, 1f, 0f)));  
        fire.setModelsPath("Models/sparks_particles.j3o");
        fire.setSpeed(5f);
        fire.setDensity(0.15f);
        fire.setSize(0.6f);
        fire.setRotation(false);
        
        stateManager.attach(fire);        
        
    }
    
    // Stop bonefire
    private void bonefireOff() {
        
        stateManager.detach(fire);

    }
    
    
    // Start volcano eruption
    private void volcanoOn() {
        
        // направление дыма 
        if(stateManager.getState(AnimationManager.class).getWindDirection()) {  // ветер с права на лево
            volcanoSmokeDirection = VOLCANO_SMOKE_DIRECTION.mult(new Vector3f(1f, 1f, 3f));
        } else {
            volcanoSmokeDirection = VOLCANO_SMOKE_DIRECTION.mult(new Vector3f(1f, 1f, -3f));
        }
        
        smoke = new ParticleEmitter3D();
        smoke.setEmitter(pivot);                                                // источник дыма
        smoke.setDirection(volcanoSmokeDirection);
        smoke.setModelsPath("Models/smoke_particles.j3o");
        smoke.setSpeed(0.05f);
        smoke.setDispersion(1f);
        smoke.setSize(0.4f);
        smoke.setMagnificationRatio(0.001f);
        smoke.setDensity(2.5f);
            
        stateManager.attach(smoke);
        
    }
    
    // Stop volcano eruption
    private void volcanoOff() {
        
        stateManager.detach(smoke);
        
    }
    
    // Controls
    public void startEruption() { this.eruption = START_STATE; }
    public void stopEruption() { this.eruption = STOP_STATE; }
    public void startBonfire() { this.burning = START_STATE; }
    public void stopBonfire() { this.burning = STOP_STATE; }
        
    
    // Getters
    public Node getEarth() { return scene; }
    
    
    @Override
    public void update(float tpf) {
        
        if(eruption == START_STATE) { 
            volcanoOn();
            eruption = WORKING_STATE;
        } else if(eruption == STOP_STATE) {
            volcanoOff();
            eruption = STOPPED_STATE;
        }    
        
        if(burning == START_STATE) { 
            bonefireOn();
            burning = WORKING_STATE;
        } else if(burning == STOP_STATE) {
            bonefireOff();
            burning = STOPPED_STATE;
        }    

    }              

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        
        super.initialize(stateManager, app);
        
        this.app = (SimpleApplication)app;
        this.stateManager = this.app.getStateManager();
        this.rootNode = this.app.getRootNode();
        this.assetManager = this.app.getAssetManager();
        
        loadModels();        
        
    }        

}
