/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package VolcanoLowPolyLW;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.light.DirectionalLight;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import java.util.ArrayList;
import java.util.List;

/*
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 *
 */

public class Balloon extends AbstractAppState {
    
    private SimpleApplication app;
    private AppStateManager stateManager;        
    private Node parent;
    private AssetManager assetManager;    
    private DirectionalLight light;
    
    private String modelsPath;                                                  // путь к файлу с моделями
    private List<Spatial> models;                                               // список с моделями 
    private List<Spatial> tmpModels;                                            // временный список моделей (нужен что бы модели не повторялись)
    private float size = DEFAULT_BALLOONSIZE;                                   // масштабирование
    
    private int balloonCount;                                                   // количество шаров
    private int balloonReady = 0;                                               // количество "запущенных" шаров
    private float update = 0f;    
    private boolean startEmission = false;
    
    private final static float DEFAULT_INTERVAL = 5f;
    
    private final static float DEFAULT_BALLOONSIZE = 1f;
    
    private final static int FAR_PLANE = -2;                                     // коридор появления 
    private final static int NEAR_PLANE = 4;
    private final static int TOP_PLANE = 8;                                 
    private final static int BOTTOM_PLANE = 4; 
    private final static float FRUSTUM_ANGLE = FastMath.tan(FastMath.DEG_TO_RAD * 45f);
    private boolean windDirection;                                              // направление движения
    
    private final static Vector3f WIND_SPEED_R2L = new Vector3f(0f, 0f, 0.5f);
    private final static Vector3f WIND_SPEED_L2R = new Vector3f(0f, 0f, -0.5f);    
    
    private final static int BALLON_MINCOUNT = 3;
    private final static int BALLON_MAXCOUNT = 5;    
    
    
    // Constructor
    public Balloon(DirectionalLight light) {
        
        this.light = light;
        
    }
    

    // контрол класс, движение облаков
    private class BalloonControl extends AbstractControl {
        
        private final Node parent;
        private Vector3f moveStep;
        private float frustum;
        
        
        // Control
        public BalloonControl(Node parent) {
            
            this.parent = parent;     
            
        }
        
        @Override 
        public void setSpatial(Spatial spatial) {
            
            super.setSpatial(spatial);
            
            // Точка за пределами фрустума для изчезновения 
            // скорость и направление полета            
            if(windDirection) {
                  moveStep = WIND_SPEED_R2L;
            } else {
                  moveStep = WIND_SPEED_L2R;                
            }               
            
            // потому что минус на минус дает плюс
            frustum = -spatial.getLocalTranslation().z;                
  
        }

        @Override
        protected void controlUpdate(float tpf) {
  
            spatial.move(moveStep.mult(tpf));
                       
            // удаление когда шар выйдет за фруструм
            if(windDirection) {
                if(spatial.getLocalTranslation().z > frustum) {
                    parent.detachChild(spatial);                    
                    balloonCount--;
                }                    
            } else {
                if(spatial.getLocalTranslation().z < frustum) {
                    parent.detachChild(spatial);                
                    balloonCount--;
                }                    
            }          
            
            // если все шары удалены (вышли за фрустум) ставим флаг об окончаниии анимации
            if(balloonCount <= 0) {
                stateManager.getState(AnimationManager.class).setShortAnimationStop();                                
            }        
        }

        @Override
        protected void controlRender(RenderManager rm, ViewPort vp) { }        
        
    }
    
    
    // Загружаем модели облаков
    private void loadModels() {
        
        Node m = (Node)assetManager.loadModel(modelsPath);
        this.models = m.getChildren();
        
    }    

    // Setters
    public void setModelsPath(String path) { this.modelsPath = path; }          // Файл с моделями    
    public void setSize(float size) { this.size = size; }                       // Масштабирование
    public void setParent(Node parent) { this.parent = parent; }                // Родитель
    
    // Control
    public void playAnimation() { 
        
        // Setup running animation flag
        stateManager.getState(AnimationManager.class).setShortAnimationRun();

        // Wind direction
        windDirection = stateManager.getState(AnimationManager.class).getWindDirection();                
        
        // Set balloon count
        balloonCount = FastMath.nextRandomInt(BALLON_MINCOUNT, BALLON_MAXCOUNT);
        
        // Copy models
        tmpModels = listClone(models);
        
        // Start
        startEmission = true;
        balloonReady = 0;
        
    }
    
    // Копирует список
    private List<Spatial> listClone(List<Spatial> src) {
        
        List<Spatial> clone = new ArrayList<Spatial>(src.size());
        
        for(Spatial s : src) {
            clone.add(s.clone());
        }
        
        return clone;
    }
    
    // Запускает шар
    private void emit() {
          
        int idx = FastMath.rand.nextInt(tmpModels.size());
        Node n = (Node)tmpModels.get(idx);
        tmpModels.remove(idx);
      
        n.setLocalTranslation(getInitialPosition());                            // точка создания
        n.setShadowMode(RenderQueue.ShadowMode.Cast);
        n.setQueueBucket(RenderQueue.Bucket.Opaque);
        
        n.rotate(0f, FastMath.rand.nextFloat(), 0f);                            // поворачиваем случайным образом
        
        if(size != DEFAULT_BALLOONSIZE) { n.scale(size); }                      // масштабируем       

        n.addControl(new BalloonControl(parent));
        n.addLight(light);
        parent.attachChild(n);        
        
    }
      
    // Точка появления облака за пределами фрустума
    private Vector3f getInitialPosition() {
        
        float x = (float)FastMath.nextRandomInt(FAR_PLANE, NEAR_PLANE);
        float y = (float)FastMath.nextRandomInt(BOTTOM_PLANE, TOP_PLANE);
        float z = (FRUSTUM_ANGLE * x) + 8f;        
        
        if(windDirection) {
            return new Vector3f(x, y, -z);
        } else {
            return new Vector3f(x, y, z);            
        }        
    }
    
    
    @Override
    public void update(float tpf) {
        
        // Запускаем шары
        if(startEmission == true) {
            if(balloonReady <= balloonCount && update > DEFAULT_INTERVAL) {
                emit();
                balloonReady++;
                update = 0f;
            }
            
            if(balloonReady >= balloonCount) {
                startEmission = false;
            }
            
            update += tpf;
        }                
    }              

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        
        super.initialize(stateManager, app);
        
        this.app = (SimpleApplication)app;
        this.stateManager = this.app.getStateManager();
        this.assetManager = this.app.getAssetManager();
        
        if(this.modelsPath == null) throw new NullPointerException("(String)path not set.");
        
        if(parent == null) { parent = this.app.getRootNode(); }
        
        loadModels();
        
    }       
}

