package VolcanoLowPolyLW;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.light.DirectionalLight;
import com.jme3.math.FastMath;
import java.util.ArrayList;
import java.util.List;

/*
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 *
 */

public class AnimationManager extends AbstractAppState {
    
    private SimpleApplication app;
    private AppStateManager stateManager;
    private DirectionalLight light;
    
    private Earth earth;
    private DinamicLowPolySky sky;
    private Clouds clouds;
    private AirPlane airPlane;
    private Balloon balloon;
    private AirShip airship;
    
    private float time = ANIMATION_LENGTH - 0.001f;
    private float shortTime = 0f;    
    private static final float ANIMATION_LENGTH = 10f;
    private static final int ANIMATION_MIN_CYCLE_COUNT = 2;
    private static final int ANIMATION_MAX_CYCLE_COUNT = 5;    
    private int LONG_ANIMATION_CYCLES = 0;
    
    private byte prevLongAnimation = ANIMATION_CLOUDS;
    private static final byte ANIMATION_VOLCANO = 0;
    private static final byte ANIMATION_CLOUDS = 1;    
    private static final byte ANIMATION_IDLE = 2;      
    
    private boolean WIND_RIGHT_TO_LEFT = false;                                         // направление ветра
    
    private boolean isShortAnimationRunning = false;
    
    
    
    public AnimationManager(DirectionalLight light) { 
    
        this.light = light;
        
    }
    
    // Start animation
    private void startAnimation() {
        
        
        /* типы анимаций:
         * 
         * 1. постоянные
         *    - вращение солнца и луны, смена дня и ночи
         *    - вращение ветряной мельницы
         * 
         * 2. периодические
         *  2.1 любое время суток:
         *      - извержение вулкана + анимация лавы
         *      - облака
         *      - пролет реактивного самолета
         * 
         *  2.2 ночные:
         *      - пролет спутника
         * 
         *  2.3 дневные:
         *      - воздушный шар
         *      - акробатический самолет
         * 
         * анимация облаков и вулкана может работать несколько анимационных циклов
         * 
         */  
        
        // todo: проблема с извержением вулкана при перемене ветра, не удаляется емиттер
        
        // ===== Long animations =====        
        --LONG_ANIMATION_CYCLES;
        if(LONG_ANIMATION_CYCLES <= 0) {
            
            // Останавливаем текущую длинную анимацию
            stateManager.getState(Earth.class).stopEruption();
            stateManager.getState(Clouds.class).stopClouds();
            
            // Новая длительность
            LONG_ANIMATION_CYCLES = FastMath.nextRandomInt(ANIMATION_MIN_CYCLE_COUNT, ANIMATION_MAX_CYCLE_COUNT);              

            // Направление ветра
            WIND_RIGHT_TO_LEFT = FastMath.rand.nextBoolean();
            
            // Выбираем длинную анимацию
            if(prevLongAnimation == ANIMATION_CLOUDS) {                         // запускаем вулкан или ничего

                if(FastMath.rand.nextBoolean()) {                                   
                    stateManager.getState(Earth.class).startEruption();                
                    prevLongAnimation = ANIMATION_VOLCANO;                    
                } else {                                                      
                    prevLongAnimation = ANIMATION_IDLE;
                }          
                
            } else if(prevLongAnimation == ANIMATION_VOLCANO) {

                if(FastMath.rand.nextBoolean()) {                                   
                    stateManager.getState(Clouds.class).startClouds();          // запускаем облака или ничего
                    prevLongAnimation = ANIMATION_CLOUDS;                    
                } else {                                                      
                    prevLongAnimation = ANIMATION_IDLE;
                }                        
                
            } else if(prevLongAnimation == ANIMATION_IDLE){                     // запускаем вулкан или облака

                if(FastMath.rand.nextBoolean()) {                                   
                    stateManager.getState(Earth.class).startEruption();                
                    prevLongAnimation = ANIMATION_VOLCANO;                                       
                } else {                                                      
                    stateManager.getState(Clouds.class).startClouds();                
                    prevLongAnimation = ANIMATION_CLOUDS;                                        
                }                        
            } 
        }
        
        // ===== Short animations =====
        // Starts only at day       
        if(sky.isDay() == true && !isShortAnimationRunning) {
            
            runRandomShortAnimation();
            
//            stateManager.getState(AirPlane.class).playRandomAnimation();                        
//            stateManager.getState(Balloon.class).playAnimation();
//            stateManager.getState(AirShip.class).playAnimation();
            

            
        }
    }
    
    // Запускаем случайную анимацию
    private void runRandomShortAnimation() {
        
        switch(FastMath.nextRandomInt(0, 3)) {
            case 0:
                stateManager.getState(AirPlane.class).playRandomAnimation();                            
                break;
            case 1:
                stateManager.getState(Balloon.class).playAnimation();                
                break;
            case 2:
                stateManager.getState(AirShip.class).playAnimation();                
                break;
            case 3:       // Idle
                break;
        }        
    } 
    
    // Анимации меньше ANIMATION_LENGTH
    private void shortAnimations() {
        // Bonefire burning only at night
        if(sky.isDay() == true && stateManager.getState(Earth.class).burning == Earth.WORKING_STATE)
            stateManager.getState(Earth.class).stopBonfire();
        
        if(sky.isDay() == false && stateManager.getState(Earth.class).burning == Earth.STOPPED_STATE)
            stateManager.getState(Earth.class).startBonfire();        
        
    }
    
    @Override
    public void update(float tpf) {
        
        if(time > ANIMATION_LENGTH) {
            
            startAnimation();
            time = 0f;
            
        }
        
        if(shortTime > ANIMATION_LENGTH / 4) {
            
            shortAnimations();
            shortTime = 0f;
            
        }
        
        time += tpf;        
        shortTime += tpf;               
    
    }        
  
    // Загружаем сцену
    private void setupScene() {
        
        // Earth
        earth = new Earth(light);
        stateManager.attach(earth);
        
        // Sky
        sky = new DinamicLowPolySky(light);
        sky.setPlanetModels("Models/planets.j3o");
        sky.setStarsTexture("Textures/stars.png");      
        stateManager.attach(sky);

        // Clouds
        clouds = new Clouds(light);
        clouds.setModelsPath("Models/clouds.j3o");
        stateManager.attach(clouds);

        // AirPlane
        airPlane = new AirPlane(light);
        airPlane.setModelsPath("Blender/Plane/plane.j3o");
        stateManager.attach(airPlane);
        
        // Hot Air Balloon
        balloon = new Balloon(light);
        balloon.setModelsPath("Models/balloon.j3o");
        stateManager.attach(balloon);
        
        // AirShip
        airship = new AirShip(light);
        airship.setModelsPath("Models/airship.j3o");
        airship.setSize(0.6f);
        stateManager.attach(airship);
        
    }
    
    // Getters
    public boolean getWindDirection() { return WIND_RIGHT_TO_LEFT; }
    
    // Setters
    public void setShortAnimationStop() { isShortAnimationRunning = false; }    
    public void setShortAnimationRun() { isShortAnimationRunning = true; }
    
    
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
    
        super.initialize(stateManager, app);
        
        this.app = (SimpleApplication)app;
        this.stateManager = this.app.getStateManager();
        
        setupScene();
        
    }
    
}
