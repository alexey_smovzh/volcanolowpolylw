/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package VolcanoLowPolyLW;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import java.util.List;

/**
 *
 * @author alex
 */
public class ParticleEmitter3D extends AbstractAppState {
    
    private SimpleApplication app;
    private Node rootNode;
    private AssetManager assetManager;
    private Node emitter;
    
    private float update = 0f;
    
    private List<Spatial> models;                                               // список с моделями частиц
    private String modelsPath;                                                  // путь к файлу с моделями
    private Vector3f direction = DEFAULT_DIRECTION;                             // вектор распространения частиц
    private float speed = DEFAULT_SPEED;                                        // скорость частиц
    private ShadowMode shadowMode = DEFAULT_SHADOWMODE;                         // режим теней, по умолчанию отключены
    private Bucket bucket = DEFAULT_BUCKET;                                     // рендер очередь, по умолчанию непрозрачные объекты
    private float size = DEFAULT_PARTICLESIZE;                                  // масштабирование частицы
    private float dispersion = DEFAULT_DISPERSION;                              // "разброс" частиц
    private float magnification = DEFAULT_MAGNIFICATION;                        // увеличение частицы при удалении от source
    private float density = DEFAULT_DENSITY;                                    // "густота" частиц
    private boolean rotation = DEFAULT_ROTATION;                                // вращать частицу?
    
    
    private static final Vector3f DEFAULT_DIRECTION = new Vector3f(0f, 4f, 0f);
    private static final float DEFAULT_SPEED = 0.1f;
    private static final ShadowMode DEFAULT_SHADOWMODE = ShadowMode.Off;
    private static final Bucket DEFAULT_BUCKET = Bucket.Opaque;
    private static final float DEFAULT_PARTICLESIZE = 1f;
    private static final float DEFAULT_DISPERSION = 0f;                     
    private static final float DEFAULT_MAGNIFICATION = 0f;
    private static final float DEFAULT_DENSITY = 1f;
    private static final boolean DEFAULT_ROTATION = false;
    

    // контрол класс, движение частицы, увеличение, изчезновение
    private class ParticleControl extends AbstractControl {
        
        private final Node parent;
        private Vector3f moveStep;
        private float scale = size;
        
        private Vector3f source;
        private Vector3f destination;
        

        
        // Control
        public ParticleControl(Node parent, Vector3f destination) {
            
            this.parent = parent;  
            this.destination = destination;
            
        }
        
        @Override 
        public void setSpatial(Spatial spatial) {
            
            super.setSpatial(spatial);

            if(dispersion != DEFAULT_DISPERSION) {
//              (FastMath.rand.nextFloat() * (max - min) + min)
                Vector3f factor = new Vector3f(1f, 1f, 1f);
                factor = factor.mult(FastMath.rand.nextFloat() * (dispersion - (-dispersion)) + (-dispersion));
                destination = destination.add(factor);                
            }
                        
            moveStep = destination.mult(speed);                                 // путь который должны пройти частицы

            source = spatial.getLocalTranslation();
            destination = source.add(destination);                              // начальная точка + путь = конечная точка

        }

        @Override
        protected void controlUpdate(float tpf) {
  
            spatial.move(moveStep.mult(tpf));
            
            if(magnification != DEFAULT_MAGNIFICATION) { 
               spatial.setLocalScale(scale);              
               scale += magnification + (tpf / 10f);
            }
            
            if(source.length() > destination.length()) {
                parent.detachChild(spatial);
            }
        }

        @Override
        protected void controlRender(RenderManager rm, ViewPort vp) { }        
        
    }
    
    
    // Вызывается из update, порождает частицы из (Node)emitter
    private void emit() {

        Node n = (Node)models.get(FastMath.rand.nextInt(models.size())).clone();
        n.setLocalTranslation(emitter.getLocalTranslation());                   // точка создания частиц
        n.setShadowMode(shadowMode);
        n.setQueueBucket(bucket);
        
        if(rotation) {
            n.rotate(FastMath.rand.nextFloat(),                                 // поворачиваем случайным образом
                     FastMath.rand.nextFloat(), 
                     FastMath.rand.nextFloat());
        }
        
        if(size != DEFAULT_PARTICLESIZE) { n.scale(size); }                     // масштабируем       

        n.addControl(new ParticleControl(emitter, direction));
        emitter.attachChild(n);
        
    }
    
    @Override
    public void update(float tpf) {
        
        if(update > density) {
            emit();
            update = 0f;
        }
        
        update += tpf;
        
    }
    
    @Override
    public void cleanup() {}


    private void loadModels() {
        
        Node m = (Node)assetManager.loadModel(modelsPath);
        this.models = m.getChildren();
        
    }
    

    // Setters
    public void setDirection(Vector3f direction) { this.direction = direction; }// Вектор распространения частиц
    public void setSize(float size) { this.size = size; }                       // Масштабирование частицы
    public void setSpeed(float speed) { this.speed = speed; }                   // Скорость частиц
    public void setEmitter(Node emitter) { this.emitter = emitter; }            // Источник частиц
    public void setModelsPath(String path) { this.modelsPath = path; }          // Файл с моделями
    public void setShadowMode(ShadowMode mode) {  this.shadowMode = mode; }     // Режим теней
    public void setBucketQueue(Bucket bucket) { this.bucket = bucket; }         // Рендер очередь
    public void setDispersion(float dispersion) { this.dispersion = dispersion; } // "разброс" частиц
    public void setMagnificationRatio(float magnification) { this.magnification = magnification; } // фактор увеличение частицы 
    public void setDensity(float density) { this.density = density; }           // "густота" частиц
    public void setRotation(boolean rotation) { this.rotation = rotation; }     // вращать частицу?
    
    
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        
        super.initialize(stateManager, app);
        
        this.app = (SimpleApplication)app;
        this.rootNode = this.app.getRootNode();
        this.assetManager = this.app.getAssetManager();
        
        if(this.emitter == null) throw new NullPointerException("(Node)emitter not set.");
        if(this.modelsPath == null) throw new NullPointerException("(String)path not set.");
        
        loadModels();
        
    }
}
