package VolcanoLowPolyLW;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.AnimEventListener;
import com.jme3.animation.LoopMode;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.light.DirectionalLight;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

/*
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 *
 */

public class AirPlane extends AbstractAppState implements AnimEventListener {

    private SimpleApplication app;
    private Node rootNode;
    private AppStateManager stateManager;
    private DirectionalLight light;
    private AssetManager assetManager;
    
    private Node model;
    private Spatial plane;
    private String modelsPath;                                                  // путь к файлу с моделями    
    
    private AnimControl control;
    private AnimChannel propeller;
    private AnimChannel aerobatic;    
    
    private static final Vector3f START_POINT = new Vector3f(2f, 4f, 0f);
    private static final String[] animations = {"Plane", "Plane2", "Plane3", "Plane4"};
    

    // Constructor
    public AirPlane(DirectionalLight light) {
        
        this.light = light;
        
    }

    private void loadModel() {
        
        // Load plane
        model = (Node)assetManager.loadModel(modelsPath);
        model.setLocalTranslation(START_POINT);
        model.setShadowMode(RenderQueue.ShadowMode.Cast);
        model.addLight(light);
        rootNode.attachChild(model);
        
        plane = model.getChild("Plane");
        
    }
    
    // Play random animation
    public void playRandomAnimation() {
        
        playAnimation(animations[FastMath.rand.nextInt(animations.length)]);
        
    }
    
    // Play named animation
    public void playAnimation(String name) {
        
        // Setup running animation flag
        stateManager.getState(AnimationManager.class).setShortAnimationRun();
        
        loadModel();
        
        // Animation
        control = plane.getControl(AnimControl.class);
        control.addListener(this);
        aerobatic = control.createChannel();        
        propeller = control.createChannel();
        aerobatic.setAnim(name);                    
        aerobatic.addBone("Plane");
        propeller.setAnim("Propeller");   
        propeller.addBone("Propeller");
        aerobatic.setLoopMode(LoopMode.DontLoop);
        propeller.setLoopMode(LoopMode.DontLoop);
        
    }

    
    public void onAnimCycleDone(AnimControl control, AnimChannel channel, String animName) {

        rootNode.detachChild(model);
        
        // Set stop short animation flag
        stateManager.getState(AnimationManager.class).setShortAnimationStop();    
        
    }

    public void onAnimChange(AnimControl control, AnimChannel channel, String animName) { }

    // Setters
    public void setModelsPath(String path) { this.modelsPath = path; }          // Файл с моделями    
    
    
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        
        super.initialize(stateManager, app);
        
        this.app = (SimpleApplication)app;
        this.stateManager = this.app.getStateManager();        
        this.rootNode = this.app.getRootNode();
        this.assetManager = this.app.getAssetManager();
        
        if(this.modelsPath == null) throw new NullPointerException("(String)path not set.");
        
    }        
}
