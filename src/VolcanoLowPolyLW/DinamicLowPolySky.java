/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package VolcanoLowPolyLW;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.shape.Sphere;
import com.jme3.util.TangentBinormalGenerator;

/*
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 *
 */
public class DinamicLowPolySky extends AbstractAppState {
    
    private SimpleApplication app;
    private Node rootNode;
    private AssetManager assetManager;    
    private Camera cam;
    
    private Node sky;
    private Node planets;
    
    private DirectionalLight light;
    private DirectionalLight planetLight;    
    
    private String texturePath;                                                 // Путь к текстуре со звездами
    private String planetsPath;                                                 // Путь к моделям планет
    private Vector3f planetsCenter = DEFAULT_PLANETSCENTER;                     // Центр вращения планет
    private Vector3f sunPosition = DEFAULT_SUNPOSITION;                         // Начальное положение солнца
    private Vector3f moonPosition = DEFAULT_MOONPOSITION;                       // Начальное положение луны
    private float planetRotSpeed = DEFAULT_PLANETSPEEDFACTOR;                   // Скорость вращения планет
    private Vector3f planetRotVector = DEFAULT_PLANETSVECTOR;                   // Вектор вращения планет
    private float skySphereRadius = DEFAULT_SKYRADIUS;                          // Радиус небесной сферы
    private ColorRGBA daySkyColor = DEFAULT_DAYSKYCOLOR;                        // Цвет неба днем
    private ColorRGBA nightSkyColor = DEFAULT_NIGHTSKYCOLOR;                    // Цвет неба ночью
    private float starsRotSpeed = DEFAULT_STARSSPEEDFACTOR;                     // Скорость вращения звезд
    private float skyChangeSpeed = DEFAULT_SKYCHANGEFACTOR;                     // Скорость смены дня и ночи
    
    
    private static final Vector3f DEFAULT_PLANETSCENTER = new Vector3f(0f, 0f, 0f);    
    private static final Vector3f DEFAULT_SUNPOSITION = new Vector3f(0f, 18f, 0f);
    private static final Vector3f DEFAULT_MOONPOSITION = new Vector3f(0f, -18f, 0f);
    private static final float DEFAULT_PLANETSPEEDFACTOR = 0.1f;
    private static final Vector3f DEFAULT_PLANETSVECTOR = Vector3f.UNIT_X;
    private static final float DEFAULT_SKYRADIUS = 60f;
    private static final ColorRGBA DEFAULT_DAYSKYCOLOR = new ColorRGBA(.5f, .5f, 1f, 1f);    
    private static final ColorRGBA DEFAULT_NIGHTSKYCOLOR = new ColorRGBA(.1f, .1f, .2f, 1f);
    private static final float DEFAULT_STARSSPEEDFACTOR = .05f;
    private static final float DEFAULT_SKYCHANGEFACTOR = .6f;    
    
    
    
    // Constructor
    public DinamicLowPolySky(DirectionalLight light) {
        
        this.light = light;
        
    }
    
   // Planet rotation and light direction control
    public class PlanetControl extends AbstractControl {
        
        private ColorRGBA color = new ColorRGBA();
        private float height;
        
        private DirectionalLight light;
        private DirectionalLight planetLight;

        private final ColorRGBA DAY_COLOR = new ColorRGBA(ColorRGBA.White);
        private final ColorRGBA NIGHT_COLOR = new ColorRGBA(ColorRGBA.DarkGray);        
        
        
        public PlanetControl(DirectionalLight light, DirectionalLight planetLight) {
            
            this.light = light;
            this.planetLight = planetLight;
            
        }

        @Override
        protected void controlUpdate(float tpf) {
            
            Vector3f position = ((Node)spatial).getChild("Sun").getWorldTranslation();
            light.setDirection(position.negate());
            
            height = position.y / 10f;
            
            color.interpolate(NIGHT_COLOR, DAY_COLOR, FastMath.sqr(height));
            light.setColor(color);
            
            planetLight.setDirection(cam.getDirection());
            
            spatial.rotate(new Quaternion().fromAngleAxis(planetRotSpeed * tpf, planetRotVector));
            
        }

        @Override
        protected void controlRender(RenderManager rm, ViewPort vp) { }
        
        public float getHeight() {
            
            return height;
            
        }        
    }
    
    // Change color in sky box
    public class SkyControl extends AbstractControl {

        private PlanetControl control;
        private ColorRGBA skyColor = new ColorRGBA(daySkyColor);
        private ColorRGBA starsColor = new ColorRGBA();
                      
        
        public SkyControl(PlanetControl control) {
            
            this.control = control;
            
        }
        
        @Override
        protected void controlUpdate(float tpf) {
            
            float height = control.getHeight();
            
            if( height > 0) {
                if(skyColor.r < daySkyColor.r) skyColor.interpolate(daySkyColor, skyChangeSpeed * tpf);                                 
                if(starsColor.a > 0f) starsColor.a -= tpf;
            } else {
                if(skyColor.r > nightSkyColor.r) skyColor.interpolate(nightSkyColor, skyChangeSpeed * tpf);
                if(starsColor.a < 0.99f) starsColor.a += tpf;
            }
            
            ((Geometry)((Node)spatial).getChild("Background")).getMaterial().setColor("Color", skyColor);
            ((Geometry)((Node)spatial).getChild("Stars")).getMaterial().setColor("Color", starsColor);
            
            ((Node)spatial).getChild("Stars").rotate(new Quaternion().fromAngleAxis(-starsRotSpeed * tpf, Vector3f.UNIT_Z));            

        }

        @Override
        protected void controlRender(RenderManager rm, ViewPort vp) {  }
        
    }
    
    // Sky
    private void setupSky(Node pivot) {        

        Geometry background = new Geometry("Background", new Sphere(8, 8, skySphereRadius));
        Material skyMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        background.setMaterial(skyMat);
        background.setShadowMode(RenderQueue.ShadowMode.Off);        
        background.setQueueBucket(RenderQueue.Bucket.Sky);
        skyMat.getAdditionalRenderState().setFaceCullMode(RenderState.FaceCullMode.Off);        
 
        pivot.attachChild(background);
        
    }
    
    // Stars
    private void setupStars(Node pivot) {        

        Sphere mesh = new Sphere(8, 8, skySphereRadius - 1f);
        Geometry stars = new Geometry("Stars", mesh);
        mesh.setTextureMode(Sphere.TextureMode.Projected);
        TangentBinormalGenerator.generate(mesh);
        
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        stars.setMaterial(mat);
        stars.setShadowMode(RenderQueue.ShadowMode.Off);        
        mat.setTexture("ColorMap", assetManager.loadTexture(texturePath));
        mat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);
        stars.setQueueBucket(RenderQueue.Bucket.Transparent);
        mat.getAdditionalRenderState().setFaceCullMode(RenderState.FaceCullMode.Off); 
        
        stars.setLocalRotation(new Quaternion().fromAngleAxis(FastMath.DEG_TO_RAD * -45f, Vector3f.UNIT_X));
 
        pivot.attachChild(stars);
        
    }

    
    private void setupPlanetSystem() {
        
        // Pivot
        planets = new Node("Pivot");
        planets.setLocalTranslation(planetsCenter);        

        // Sun & Moon
        Node models = (Node)assetManager.loadModel(planetsPath);
        Spatial sun = models.getChild("Sun");
        Spatial moon = models.getChild("Moon");        
        Spatial ring = models.getChild("Asteroid");
        
        AnimControl control = ring.getControl(AnimControl.class);
        AnimChannel channel = control.createChannel();
        channel.setAnim("Asteroid");                

        // Light for sun & moon mesh        
        planetLight = new DirectionalLight();
        planets.addLight(planetLight);            
        
        // Positioning        
        sun.setLocalTranslation(sunPosition);
        moon.setLocalTranslation(moonPosition);
        ring.setLocalTranslation(moonPosition);
        planets.attachChild(sun);
        planets.attachChild(moon);
        planets.attachChild(ring);
        rootNode.attachChild(planets);
        
    }
    
    private void loadModels() {
        
        // Sky
        sky = new Node("Sky");
        sky.setLocalTranslation(Vector3f.ZERO);
        rootNode.attachChild(sky);
        
        setupSky(sky);
        setupStars(sky);
        setupPlanetSystem();      
        
        // Control
        planets.addControl(new PlanetControl(light, planetLight));
        sky.addControl(new SkyControl(planets.getControl(PlanetControl.class)));      
        
    }
    
    
    @Override
    public void update(float tpf) {        
   
    }
    
    @Override
    public void cleanup() {}

   
    // Setters
    public void setStarsTexture(String texturePath) { this.texturePath = texturePath; }     // текстура звезд
    public void setPlanetModels(String planetsPath) { this.planetsPath = planetsPath; }     // модели планет
    public void setPlanetsCenter(Vector3f center) { this.planetsCenter = center; }
    public void setSunPosition(Vector3f position) { this.sunPosition = position; }
    public void setMoonPosition(Vector3f position) { this.moonPosition = position; }    
    public void setPlanetRotSpeed(float speed) { this.planetRotSpeed = speed; }             // Скорость вращения планет
    public void setPlanetRotVector(Vector3f vector) {this.planetRotVector = vector; }       // Вектор вращения планет
    public void setDaySkyColor(ColorRGBA color) { this.daySkyColor = color; }               // Цвет неба днем
    public void setNightSkyColor(ColorRGBA color) { this.nightSkyColor = color; }           // Цвет неба ночью
    public void setStarsRotSpeed(float speed) { this.starsRotSpeed = speed; }               // Скорость вращения звезд
    public void setSkyChangeSpeed(float speed) { this.skyChangeSpeed = speed; }             // Скорость смены дня и ночи
    
    // Getters
    public boolean isDay() { 

        if(planets.getControl(PlanetControl.class).getHeight() > 0f) {
            return true;
        } else {
            return false;
        }      
        
    }
    
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        
        super.initialize(stateManager, app);
        
        this.app = (SimpleApplication)app;
        this.rootNode = this.app.getRootNode();
        this.assetManager = this.app.getAssetManager();
        this.cam = this.app.getCamera();
        
        if(this.texturePath == null) throw new NullPointerException("(String)texturePath not set.");
        if(this.planetsPath == null) throw new NullPointerException("(String)planetsPath not set.");
        
        
        loadModels();
        
    }
}
