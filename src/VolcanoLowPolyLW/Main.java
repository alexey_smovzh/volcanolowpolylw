package VolcanoLowPolyLW;

import com.jme3.app.SimpleApplication;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Node;
import com.jme3.shadow.DirectionalLightShadowRenderer;
import com.jme3.system.AppSettings;

/*
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 *
 */

/*
 * F5 - show hide fps screen
 * C - print camera position
 * M - print memory heap
 */

/* todo:
 * 
 * отредактировать лаву и перекос влево земли
 * разобраться с поверхностями принимающими, бросающими тени
 * разобраться со светом.....
 * 
 */

// todo: считаем fps, в зависимости от результата показываем:
//                  - нормальные тени
//                  - тени хуже
//                  - без теней

public class Main extends SimpleApplication {

    Node scene;

    private DirectionalLight light;     
    private static final int SHADOW_MAP_SIZE = 1024;    
    
    
    @Override
    public void simpleInitApp() {
                
        setupCam();
        setupLight();        

        // Wallpaper animation manager
        AnimationManager manager = new AnimationManager(light);
        stateManager.attach(manager);
        
    }   
    
    @Override
    public void simpleUpdate(float tpf) {
        //TODO: add update code
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
    
    private void setupCam() {
        
        flyCam.setMoveSpeed(20f);
        
        cam.setLocation(new Vector3f(30.165884f, 2.0726776f, -4.611283f));
        cam.setRotation(new Quaternion(-0.06264074f, -0.6466378f, -0.053414125f, 0.758342f));

    }
    
    private void setupLight() {

        AmbientLight ambient = new AmbientLight();
        ambient.setColor(ColorRGBA.White.mult(0.5f));
        rootNode.addLight(ambient);

        light = new DirectionalLight();   
        
        if(System.getProperty("java.vm.name").equals("Dalvik") == false) {
        
            DirectionalLightShadowRenderer dlsr = new DirectionalLightShadowRenderer(assetManager, SHADOW_MAP_SIZE, 4);
            dlsr.setLight(light);        
            viewPort.addProcessor(dlsr);
        
        }
        // if system is Android set specific shadow params
/*        if(System.getProperty("java.vm.name").equals("Dalvik")) {
            dlsr.setLambda(0.55f);
            dlsr.setShadowIntensity(0.55f);
            dlsr.setShadowCompareMode(com.jme3.shadow.CompareMode.Software);
            dlsr.setEdgeFilteringMode(EdgeFilteringMode.PCF4);
        }  */
              
    }    

    
    
    public static void main(String[] args) {
         
        Main app = new Main();
                       
        AppSettings settings = new AppSettings(true);
        settings.setResolution(360, 640);                   // original 540, 960
        settings.setSamples(4);
        
        settings.setFrameRate(20);
        
        app.setSettings(settings);
        app.setShowSettings(false);
        
        app.start();

     }
}
